var express = require('express');
var router = express.Router();
// const qrcodeterminal = require('qrcode-terminal');
// const {Server} = require("socket.io");
// socketServer = new Server(8000);//el server debe estar iniciado afuera ya que solo se gestiona los events sockerServer.on(connection) y cada socker su propio metodo disconnect
const WhatssappClient = require("../models/whatsappClient");
const Manager = require("../models/manager");
const Consumer = require("../models/consumer");
const QRCode = require('qrcode');
// const { MessageMedia } = require('whatsapp-web.js');
const clientsArray=[];
const timeoutTimes=4;
var tempClient;
let sequenceNumberByClient = new Map();
let currentClients = new Map();
let consumer = new Consumer();
// const { PassThrough } = require('stream');
// router.get('/qr/:content', async (req, res, next) => {
//     try{
//         const content = req.params.content;            
//         const qrStream = new PassThrough();
//         const result = await QRCode.toFileStream(qrStream, content,
//                     {
//                         type: 'png',
//                         width: 200,
//                         errorCorrectionLevel: 'H'
//                     }
//                 );

//         qrStream.pipe(res);
//     } catch(err){
//         console.error('Failed to return content', err);
//     }
// })
router.get('/getQrAsImage', async (req, res, next) => {
    try{
        tempClient = consumer.getManager().start(req.body.id);
        tempClient.instanceNewSession(req.app.get("socketapi"));
        tempClient.getQrAsImage().then((qrStream)=>{
            qrStream.pipe(res);
        })
    } catch(err){
        console.error('Failed to return content', err);
    }
})
router.get("/getQrAsBase64", function(req, res) {
    // console.info("body: ",req.body);
    res.setHeader('Content-Type', 'application/json');
    tempClient = consumer.getManager().start(req.body.id);
    tempClient.instanceNewSession(req.app.get("socketapi"));
    tempClient.getQrAsBase64().then((data)=>{
        res.status(200).send(JSON.stringify({status:"OK",data:data}));        
    });
})
////////////////////////Manager methods////////////////
router.get('/managerlist', function(req, res) {
    // console.log("managerlist");
    res.setHeader('Content-Type', 'application/json');
    console.log(Array.from(consumer.getManager().listAll()));
    res.status(200).send(JSON.stringify({status:"OK",instances:Array.from(consumer.getManager().listAll())}));        
})
////////////////////////CONTACT MESSAGE////////////////
router.post("/sendMessageText", function(req, res) {
    // console.info("body: ",req.body);
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to.substring(1) + "@c.us";
    consumer.getManager().get(req.body.instanceid).sendMessageText(chatId, req.body.msg);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageMediaUrl", function(req, res) {
    // uri="https://firebasestorage.googleapis.com/v0/b/personalizados-web.appspot.com/o/WhatsApp%20Video%202021-07-01%20at%203.41.48%20PM.mp4?alt=media&token=077708dc-78f0-4d12-bdbc-d5902648d300";
    // uri="https://dirtbikemagazine.com/wp-content/uploads/2020/07/segwaye.jpg";
    // let media = MessageMedia.fromUrl("https://www.redipd.org/sites/default/files/2020-02/xvi-encuentro-Rafael-Perez.pdf").then((resp)=>{
    // console.info("body: ",req.body);
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to.substring(1) + "@c.us";
//uri="https://firebasestorage.googleapis.com/v0/b/gobernacrm.appspot.com/o/multimedia%2FfileStiker.txt?alt=media&token=11d68105-dd6f-4bd8-b83f-2e02529f358a";
    consumer.getManager().get(req.body.instanceid).sendMessageMediaUrl(chatId,req.body.uri,req.body.msg,req.body.filename);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageStickerUrl", function(req, res) {
    // uri="https://firebasestorage.googleapis.com/v0/b/personalizados-web.appspot.com/o/WhatsApp%20Video%202021-07-01%20at%203.41.48%20PM.mp4?alt=media&token=077708dc-78f0-4d12-bdbc-d5902648d300"
    // uri="https://dirtbikemagazine.com/wp-content/uploads/2020/07/segwaye.jpg";
    // let media = MessageMedia.fromUrl("https://www.redipd.org/sites/default/files/2020-02/xvi-encuentro-Rafael-Perez.pdf").then((resp)=>{
    // console.info("body: ",req.body);
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to.substring(1) + "@c.us";
//uri="https://firebasestorage.googleapis.com/v0/b/gobernacrm.appspot.com/o/multimedia%2F9dfbc974-bef4-4d95-93c5-d8d8c834d6cf.webp?alt=media&token=892a19be-5538-4dc3-9a53-1b2ccbce5d84"
    consumer.getManager().get(req.body.instanceid).sendMessageStickerUrl(chatId,req.body.uri,req.body.msg);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageFileUrl", function(req, res) {
    // console.info("body: ",req.body);
    // let media = MessageMedia.fromUrl("https://www.redipd.org/sites/default/files/2020-02/xvi-encuentro-Rafael-Perez.pdf").then((resp)=>{
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to.substring(1) + "@c.us";
//uri="https://dirtbikemagazine.com/wp-content/uploads/2020/07/segwaye.jpg";
    consumer.getManager().get(req.body.instanceid).sendMessageFileUrl(chatId,req.body.uri,req.body.msg,req.body.filename);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
////////////////////////GROUP MESSAGE////////////////
router.post("/sendMessageTextGroup", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageText(chatId, req.body.msg);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageMediaUrlGroup", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageMediaUrl(chatId,req.body.uri,req.body.msg,req.body.filename);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageStickerUrlGroup", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageStickerUrl(chatId,req.body.uri,req.body.msg);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageFileUrlGroup", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageFileUrl(chatId,req.body.uri,req.body.msg,req.body.filename);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
////////////////////////GROUP MESSAGE////////////////
router.post("/sendMessageTextBroadcast", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageText(chatId, req.body.msg);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageMediaUrlBroadcast", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageMediaUrl(chatId,req.body.uri,req.body.msg,req.body.filename);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageStickerUrlBroadcast", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageStickerUrl(chatId,req.body.uri,req.body.msg);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/sendMessageFileUrlBroadcast", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const chatId = req.body.to;
    consumer.getManager().get(req.body.instanceid).sendMessageFileUrl(chatId,req.body.uri,req.body.msg,req.body.filename);
    res.status(200).send(JSON.stringify({status:"OK"}));
})
router.post("/getStatus", function(req, res) {
    // console.info("body: ",req.body)
    /////BUSCA EN EL ARRAY DE CLIENTES
    
    /////INSTACIAR UN NUEVO CLIENTE SI NO ENCUENTRA EN EL ARRAY DE CLIENTES
    // client=clientStart(null,req.body);
    // client.on('authenticated', (session) => {    
        //     console.info("getStatus: ",session)
        //     res.status(200).send(JSON.stringify({status:"OK",data:session}));
        // });
        // tempClient.getStatus()
        // client.on('ready', () => {    
            //     console.info("getStatus: ",session)
            //     res.status(200).send(JSON.stringify({status:"OK",data:session}));
            // });
    res.setHeader('Content-Type', 'application/json');
    if(consumer.getManager().get(req.body.instanceid)){
        consumer.getManager().get(req.body.instanceid).getStatus()
        .then((resolve)=>{
            res.status(200).send(JSON.stringify({status:"OK",data:resolve}));
        })
        .catch((reject)=>{
            res.status(200).send(JSON.stringify({status:"WARN",data:reject}));
        })
    }else{
        res.status(200).send(JSON.stringify({status:"WARN",data:{state:"Cliente no iniciado"}}));
    }
})
router.post("/logout", function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    if(consumer.getManager().get(req.body.instanceid)){
        consumer.getManager().get(req.body.instanceid).logout()
        .then((resolve)=>{
            res.status(200).send(JSON.stringify({status:"OK",data:resolve}));
        })
        .catch((reject)=>{
            res.status(500).send(JSON.stringify({status:"WARN",data:reject}));
        })
    }else{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Cliente no iniciado"}));
    }
})
router.post("/sincronize", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    tempClient = consumer.getManager().start(req.body.instanceid);
    tempClient.instanceNewSession(req.app.get("socketapi")).then((resolve)=>{
        // res.status(200).send(JSON.stringify({status:"OK",data:"instanceNewSession"}));
        tempClient.getStatus().then((resolve)=>{
            console.log("sincronizeSUCCESS: ",resolve);
            res.status(200).send(JSON.stringify({status:"OK",data:resolve}));
        }).catch((reject)=>{
            console.log("sincronizeERROR: ",reject);
            res.status(500).send(JSON.stringify({status:"ERROR",data:reject}));
        })
    }).catch((reject)=>{
        console.log("sincronizeERROR: ",reject);
        res.status(500).send(JSON.stringify({status:"ERROR",data:reject}));
    });
});
router.post("/restoreClient", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    tempClient = consumer.getManager().start(req.body.instanceid);
    tempClient.instanceExistSession(req.app.get("socketapi"),req.body.usuarioID).then((resolve)=>{
        tempClient.getStatus().then((resolve)=>{
            console.log("restoreClientSUCCESS: ",resolve);
            res.status(200).send(JSON.stringify({status:"OK",data:resolve}));
        }).catch((reject)=>{
            console.log("restoreClientERROR: ",reject);
            res.status(500).send(JSON.stringify({status:"ERROR",data:reject}));
        })
    }).catch((reject)=>{
        console.log("restoreClientERROR: ",reject);
        res.status(500).send(JSON.stringify({status:"ERROR",data:reject}));
    });
});
router.post("/setStatus", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    consumer.getManager().get(req.body.instanceid).setStatus(req.body.status).then(()=>{
        res.status(200).send(JSON.stringify({status:"OK",data:"Estado actualizado"}));
    }).catch(()=>{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Error actualizando estado"}));
    });
});
router.get("/getProfilePicUrl", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    consumer.getManager().get(req.body.instanceid).getProfilePicUrl(req.body.status).then((data)=>{
        res.status(200).send(JSON.stringify({status:"OK",data:data}));
    }).catch(()=>{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Error actualizando estado"}));
    });
});
router.get("/getChats", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    consumer.getManager().get(req.body.instanceid).getChats(req.body.status).then((data)=>{
        res.status(200).send(JSON.stringify({status:"OK",data:data}));
    }).catch(()=>{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Error obteniendo chats"}));
    });
});
router.get("/getGroups", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    consumer.getManager().get(req.body.instanceid).getGroups(req.body.status).then((data)=>{
        res.status(200).send(JSON.stringify({status:"OK",data:data}));
    }).catch(()=>{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Error obteniendo chats"}));
    });
});
router.get("/getBroadcast", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    consumer.getManager().get(req.body.instanceid).getBroadcast(req.body.status).then((data)=>{
        res.status(200).send(JSON.stringify({status:"OK",data:data}));
    }).catch(()=>{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Error obteniendo chats"}));
    });
});
router.post("/setDisplayName", function(req, res) {
    // req.app.get("socketapi").disconnect();
    res.setHeader('Content-Type', 'application/json');
    consumer.getManager().get(req.body.instanceid).setDisplayName(req.body.displayName).then(()=>{
        res.status(200).send(JSON.stringify({status:"OK",data:"Estado actualizado"}));
    }).catch(()=>{
        res.status(500).send(JSON.stringify({status:"ERROR",data:"Error actualizando estado"}));
    });
});

/////////////////////////////////////////////////////////////////////////////////////////
function socketStart(socket){
    // return new Promise((resolve)=>{        
        // event fired every time a new client connects:
        // socketServer.on("connection", (socket) => {
            // console.info(`Client connected [id=${socket.id}]`);
            let client=clientStart(socket);
            // client.on('authenticated', (session) => {   
            //     socket.emit("authenticated", true);
            //     socket.emit("sessionToken", session);
            // });
            // client.on('ready',()=>{
            //     socket.emit("clientInfo", client.info);
            //     currentClients.set(client.info.wid.user,client);
            // });
            // client.on('message', async msg => {
            //     socket.emit("message", msg);
            //     if(msg.hasMedia) {
            //         const media = await msg.downloadMedia();
            //         msg.reply(media);
            //         // do something with the media data here
            //     }
            //     // if (msg.body == '!ping') {
            //     //     msg.reply('pong');
            //     // }
            // });
            // client.on("message_ack", async (msg, ack) => {
            //     // socket.emit("messageAck", {msg:msg, ack:ack});
            //     console.log(msg); 
            //     console.log(ack);
            // })
            socket.on("disconnect", async() => {
                // sequenceNumberByClient.delete(socket);
                await client.destroy().then(
                    resp=>console.info("desconecto")
                ).catch(
                    err=>console.info("errorCERRANDO")
                );
            });
        // });
        // resolve(true);
    // })
}
// function clientStart(socket,session){
//     var currentUser;
//     contador=0;
//     client = new Client({
//         // ffmpegPath: 'C:/ffmpeg/bin/ffmpeg.exe',
//         session:session,
//         puppeteer: {//no envia videos sin codec
//             // executablePath: 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe',
//             executablePath: '/usr/bin/google-chrome-stable',
//             args: ['--no-sandbox']
//         }
//     })
//     //si no existe session inici la peticion de qr
//     if (session === undefined || session === null) {
//         client.on('qr', (qr) => {
//             sequenceNumberByClient.set(socket, {client:client,socket:socket,qr:qr,contador:contador});
//             socket.emit("generateQR", qr);
//             console.log('QR GENERATED: ',contador," HASH:", qr);
//             if(sequenceNumberByClient.get(socket).contador>=timeoutTimes){
//                 socket.emit('closeReason','timeout');
//                 sequenceNumberByClient.get(socket).client.destroy();
//                 sequenceNumberByClient.get(socket).socket.disconnect();
//             }
//             contador++;
//         });
//    }
//     client.initialize();
//     clientsArray.push(client);
//     return client;
// }
function clientDestroy(client){
    client.destroy();
}
function clientLogout(client){
    client.logout();
}
router.get("/send", function(req, res) {

});

module.exports = router;
