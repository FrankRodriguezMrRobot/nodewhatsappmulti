var createError = require('http-errors');
var express = require('express');
var socketapi = require("./socketapi"); // <== Add this line
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors'); 

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

// tutorial
var personsRouter = require('./routes/persons');
var whatsappRouter = require('./routes/whatsapp');
const Consumer = require('./models/consumer');

var app = express();
// var appSocket = express();
app.use(cors({
  // origin: ['https://sunatickets.web.app','http://localhost','http://localhost:4200']
  origin: '*'
}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// tutorial
app.use('/persons', personsRouter);
app.use('/whatsapp', whatsappRouter);
// appSocket.use('whatsapp',whatsappRouter);
/////////////////////////////////
//FUNCIONES EXTRAS///////////////
/////////////////////////////////
// try{
//   tempProducer=new Consumer();
//   // tempProducer.activate();
// } catch(err){
//   console.error('Failed to init consumer', err);
// }
/////////////////////////////////
//FUNCIONES EXTRAS///////////////
/////////////////////////////////
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = { app, socketapi };
