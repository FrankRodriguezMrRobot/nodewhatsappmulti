const WhatssappClient = require('./whatsappClient');
const uuidv4 = require('uuid/v4');


class Manager{
    constructor(){
        this.list=new Map();
    }
    start(id){
        // if(this.list.get(id)){
        //     console.log("client exist");
        //     return this.get(id);
        // }else{
        //     console.log("client not exist");
        //     let tempClient = new WhatssappClient();
        //     // let uuid=uuidv4();
        //     this.list.set(id, tempClient);
        //     return tempClient;
        // }
        let tempClient = new WhatssappClient();
        // let uuid=uuidv4();
        this.list.set(id, tempClient);
        return tempClient;
    }
    stop(){

    }
    listAll(){
        return this.list.keys();
    }
    get(id){
        return this.list.get(id);
    }
    reset(){

    }
    reload(){

    }
    clearAll(){

    }
    startAll(){

    }
}
module.exports=Manager;