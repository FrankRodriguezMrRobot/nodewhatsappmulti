const stompit = require('stompit');
const dotenv = require('dotenv');
const Manager = require('./manager');

dotenv.config({ path: "/home/ubuntu/nodewhatsappmulti/.env" });
const connectOptions = {
  'host': process.env.BROKER_HOST,
  'port': process.env.BROKER_PORT,
  'connectHeaders':{
    'host': '/',
    'login': process.env.BROKER_USER,
    'passcode': process.env.BROKER_PASS,
    'heart-beat': '5000,5000'
  }
};
class Consumer{//actualemnte esta abriendo n conexiones por n clientes instanciados N consumers, la correcion es solo consumer y por id obtener el cliente 
  constructor(){
    this.manager=new Manager();
    // this.whatsappClient=client;
    // console.log("consumerWhatsapp: ",client);
    stompit.connect(connectOptions, (error, tempClient) => {
      this.client = tempClient;
      console.log("constructor Consumer");
      if (error) {
        console.log('connect error ' + error.message);
        return;
      }
      //CONSUMIR PARAMETROS
      var subscribeParams = {
        'destination': '/'+process.env.INSTANCIA_URL+'/'+process.env.TOKEN_URL,
        'ack': 'client-individual'
      };
      this.client.subscribe(subscribeParams, (error, message) => {    
        if (error) {
          console.log('subscribe error ' + error.message);
          return;
        }
        message.readString('utf-8', (error, body) => { 
          if (error) {
            console.log('read message error ' + error.message);
            return;
          }
          console.log('received message: ' + body);
          let dataReceived=JSON.parse(body);
          this.sendEvents(dataReceived.method,dataReceived);
          // this.client.ack(message);
          // this.client.disconnect();
        });

      });
    });


  }
  // getClient(){
  //   return this.whatsappClient;
  // }
  sendEvents(event, data){
    const chatId = data.to.substring(1) + "@c.us";
    switch (event) {
      case 'sendMessageText':
        try{
          this.manager.get(data.instanceid).sendMessageText(chatId, data.msg);
          console.log('sendMessageTextEVENT');
        }catch(error){
          console.log(error);
        }
        break;
      case 'sendMessageMediaUrl':
        try{
          this.manager.get(data.instanceid).sendMessageMediaUrl(chatId, data.uri, data.msg, data.filename);
          console.log('sendMessageMediaUrlEVENT');
        }catch(error){
          console.log(error);
        }
        break;
      case 'sendMessageStickerUrl':
        try{
          this.manager.get(data.instanceid).sendMessageStickerUrl(chatId, data.uri, data.msg);
          console.log('sendMessageStickerUrlEVENT');
        }catch(error){
          console.log(error);
        }
        break;
      case 'sendMessageFileUrl':
        try{
          this.manager.get(data.instanceid).sendMessageFileUrl(chatId, data.uri, data.filename);
          console.log('sendMessageFileUrlEVENT');
        }catch(error){
          console.log(error);
        }
        break;
    }
  }

  getManager(){
    return this.manager;
  }
}
module.exports=Consumer;