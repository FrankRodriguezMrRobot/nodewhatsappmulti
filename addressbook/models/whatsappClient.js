const { Client } = require('whatsapp-web.js');
const { ClientInfo } = require('whatsapp-web.js');
const qrcode = require('qrcode');
const { MessageMedia } = require('whatsapp-web.js');
const { PassThrough } = require('stream');
// jsonToFormData = require('../utils.js');
const axios = require('axios');
const dotenv = require('dotenv');
// const Consumer = require('../models/consumer');
dotenv.config({ path: "/home/ubuntu/nodewhatsappmulti/.env" });
// console.log("DIRNAME. ",`${__dirname}/../../.env`)
//UPLOAD ROUTE FOR MESSAGE MEDIA año/mes
// class WhatssappClient{}
class WhatssappClient{
    constructor(){}
    instanceNewSession(socket_p){
        return new Promise((resolve,reject)=>{
            this.state="init";
            this.contador=0;
            this.socket=socket_p;//socketServe
            this.session=null;
            this.timeoutTimes=2;
            this.client = new Client({
                puppeteer: {
                    executablePath: '/usr/bin/google-chrome-stable',
                    args: ['--no-sandbox']
                }
            })
            this._initEventos();
            this.client.initialize().then(()=>{
                // this.tempConsumer=new Consumer(this);
                console.log("instanceNewSession SUCCESS");
                resolve("OK")
            }).catch(()=>{
                console.log("instanceNewSession ERROR");
                reject("ERROR");
            });
        })
    }
    instanceExistSession(socket_p, usuarioID){
        return new Promise((resolve,reject)=>{
            this.state="init";
            this.contador=0;
            this.socket=socket_p;
            // this.session=session_p;
            this.timeoutTimes=2;
            this.whabizGetSession(usuarioID).then((res)=>{
                this.session = JSON.parse(res.data.wha_hash);
                console.log("this.session: ",this.session);
                this.client = new Client({
                    session:this.session,
                    puppeteer: {
                        executablePath: '/usr/bin/google-chrome-stable',
                        args: ['--no-sandbox']
                    }
                })
                this._initEventos();
                this.client.initialize().then(()=>{
                    // this.tempConsumer=new Consumer(this.client);
                    console.log("instanceExistSession SUCCESS");
                    resolve("OK");
                }).catch(()=>{
                    console.log("instanceExistSession ERROR");
                    reject("ERROR");
                });
            })
        })
    }
    getState(state){
        var states = new Map([
            ["CONNECTED", "Conectado"],
            ["DISCONNECTED", "Desconectado"],
            ["TIMEOUT", "Expirado"],
            ["CONFLICT","Conflicto"],
            ["OPENING","Abriendo"],
            ["PAIRING","Emparejando"]
          ]);
          return states.get(state)
    }
    whabizSaveSession(hash){
        let body={
            instancia: process.env.INSTANCIA_URL,
            token: process.env.TOKEN_URL,
            fechaHora: Date.now(),
            hash: JSON.stringify(hash)
        }
        // axios.post(process.env.BUSSINESS_URL+'/Whabiz/Session/guardar', formData, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        axios.post(process.env.BUSSINESS_URL+'/Whabiz/Session/guardar2', body)
        .then(res => {

        })
        .catch(error => {
            console.error(error)
        })
    }
    whabizGetSession(usuarioID){
        return new Promise((resolve,reject)=>{
            let body={
                instancia: process.env.INSTANCIA_URL,
                //token: process.env.TOKEN_URL,
                usuarioID:usuarioID
            }
            console.log("whabizGetSession: ",body);
            // axios.post(process.env.BUSSINESS_URL+'/Whabiz/Session/guardar', formData, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            http://hostname:8161/hawtio/jolokia/exec/org.apache.activemq:type=Broker,brokerName=MyBroker/removeQueue(java.lang.String)/MyQueue
            axios.post(process.env.ERP_URL+'/Utils/Envios/getStatusNew', body)
            .then(res => {
                // console.log(`whabizGetSession: ${res.status}`,res);
                // console.log("whabizGetSessionDATA: ",res);
                resolve(res);
            })
            .catch(error => {
                console.error(error)
                reject(error);
            })
        })
    }
    _initEventos(){
        /**
         * ALL EVENTS
         * 
auth_failure
authenticated
change_battery
change_state
disconnected
group_join
group_leave
group_update
incoming_call
media_uploaded
message
message_ack
message_create
message_revoke_everyone
message_revoke_me
qr
ready
         */
        /** 
         * auth_failure
         */
         this.client.on('auth_failure', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("auth_failure: ",data):{};
            this.socket.emit("auth_failure", data);
        });
        /** 
         * authenticated
         *  */ 
        this.client.on('authenticated', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("authenticated: ",data):{};
            this.state="authenticated";
            this.whabizSaveSession(data);
            this.socket.emit("authenticated", data);
        });
        /** 
         * change_battery
         *  */ 
        this.client.on('change_battery', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("change_battery: ",data):{};
            this.socket.emit("change_battery", data);
        });
        /** 
         * change_state
         *  */ 
        this.client.on('change_state', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("change_state: ",data):{};
            this.socket.emit("change_state", this.getState(data));
        });
        /** 
         * disconnected
         *  */ 
        this.client.on('disconnected', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("disconnected: ",data):{};
            this.state="disconnected";
            this.socket.emit("disconnected", this.getState(data));
        });
        /** 
         * group_join
         *  */ 
        this.client.on('group_join', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("group_join: ",data):{};
            this.socket.emit("group_join", data);
        });
        /** 
         * group_leave
         *  */ 
        this.client.on('group_leave', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("group_leave: ",data):{};
            this.socket.emit("group_leave", data);
        });
        /** 
         * group_update
         *  */ 
        this.client.on('group_update', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("group_update: ",data):{};
            this.socket.emit("group_update", data);
        });
        /** 
         * incoming_call
         *  */ 
        this.client.on('incoming_call', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("incoming_call: ",data):{};
            this.socket.emit("incoming_call", data);
        });
        /** 
         * media_uploaded
         *  */ 
        this.client.on('media_uploaded', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("media_uploaded: ",data):{};
            this.socket.emit("media_uploaded", data);
        });
        /** 
         * message
         *  */ 
        this.client.on('message', (data) => {   
            /////////message media///////
            if (data.hasMedia) {
                process.env.SHOW_CONSOLE!='0'?console.log("INSIDE"):{};
                data.downloadMedia().then((media)=>{
                    process.env.SHOW_CONSOLE!='0'?console.log("mediaMessage: ",media):{};
                });
            }
            //////////////////////////
            process.env.SHOW_CONSOLE!='0'?console.log("message: ",data):{};
            // axios.post(process.env.BUSSINESS_URL+'/WebHookDesarrolloMessageReceived', {data:data})
            // .then(res => {
    
            // })
            this.socket.emit("message", data);
        });
        /** 
         * group_update
         *  */ 
         this.client.on("message_ack", async (msg, ack) => {
             process.env.SHOW_CONSOLE!='0'?console.log("message_ack: ",msg):{};
             process.env.SHOW_CONSOLE!='0'?console.log("message_ack: ",ack):{};
             this.socket.emit("message_ack", {msg:msg,ack:ack});
        })
        /** 
         * message_create
         *  */ 
        this.client.on('message_create', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("message_create: ",data):{};
            this.socket.emit("message_create", data);
        });
        /** 
         * message_revoke_everyone
         *  */ 
        this.client.on('message_revoke_everyone', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("message_revoke_everyone: ",data):{};
            this.socket.emit("message_revoke_everyone", data);
        });
        /** 
         * message_revoke_me
         *  */ 
        this.client.on('message_revoke_me', (data) => {   
            process.env.SHOW_CONSOLE!='0'?console.log("message_revoke_me: ",data):{};
            this.socket.emit("message_revoke_me", data);
        });
        /** 
         * qr
         *  */ 
         if (this.session === undefined || this.session === null) {
            this.client.on('qr', (qr) => {
                // sequenceNumberByClient.set(socket, {client:client,socket:socket,qr:qr,contador:contador});
                process.env.SHOW_CONSOLE!='0'?console.log('qr: ',this.contador," HASH:", qr):{};
                if(this.contador>=this.timeoutTimes){
                    this.socketEmit('qr_expired','timeout');
                    this.socketEmit('close_reason','timeout');
                    this.client.destroy();
                    this.socketDisconnect(this.socket);//mata a todos los socket que esten conectados escuchando socketServer
                }else{
                    this.encodeBase64(qr).then((base64)=>{
                        this.socketEmit("qr", base64);
                    });
                }
                this.contador++;
            });
        }
        /** 
         * ready
         */
        this.client.on('ready',()=>{
            process.env.SHOW_CONSOLE!='0'?console.log("ready: "):{};
            this.socket.emit("ready", this.client.log);
            // currentClients.set(client.log.wid.user,client);
        });
    }
    initializeClient(){
        this.client.initialize();
    }
    logout(){
        return new Promise((resolve,reject)=>{
            this.client.logout().then(()=>{
                console.log("client logout");
                this.client.destroy().then(()=>{
                    this.socketDisconnect(this.socket);//mata a todos los socket que esten conectados escuchando socketServer
                    console.log("client destroy");
                    resolve("Cliente eliminado");
                }).catch(()=>{
                    console.log("client NOT destroy");
                    reject("No se pudo eliminar cliente");
                });
            }).catch(()=>{
                console.log("client NOT logout");
                reject("No se pudo cerrar sesion");
            })

        })
    }
    socketEmit(theme,data){
        if(this.socket){
            this.socket.emit(theme,data);
        }else{
            console.log("No socket for client: ",{client:client,theme:theme,data:data});
        }
    }
    socketDisconnect(socketServer){
        function getConnectedSockets() {
            return Array.from(socketServer);
        }
        
        getConnectedSockets().forEach(function(s) {
            s.disconnect(true);
        });
        // if(socket_){
        //     socket_.disconnect();
        // }else{
        //     console.log("Disconnect socket: ",{client:client});
        // }
    }
    encodeBase64(text){
        return new Promise((resolve,reject)=>{        
            resolve(
                qrcode.toDataURL(text)
            )
        })
    }
    /**
     * 
     * 
     * QR METHODS
     * 
     */
     getQrRaw(){
        return new Promise((resolve,reject)=>{        
            this.client.on('qr', (qr) => {
                if(this.contador>=this.timeoutTimes){
                    this.client.destroy();
                }else{
                    console.log('qr: ',this.contador," HASH:", qr);
                    resolve(qr);
                }
                this.contador++;
            });
            this.initializeClient();
        })
    }
     getQrAsImage(res){//NO SE USA YA QUE RETORNA LO MISMO QUE getQrRaw
        return new Promise((resolve,reject)=>{        
            this.client.on('qr', async (qr) => {
                if(this.contador>=this.timeoutTimes){
                    this.client.destroy();
                }else{
                    console.log('qr: ',this.contador," HASH:", qr);
                    const qrStream = new PassThrough();
                    // tempClient.getQrRaw().then(async(qr)=>{
                    const result = await qrcode.toFileStream(qrStream, qr,{type: 'png',width: 200,errorCorrectionLevel: 'H'});
                    // qrStream.pipe(res);
                    // })
                    resolve(qrStream);//resolve raw stirng
                }
                this.contador++;
            });
            this.initializeClient();
        })
    }
    getQrAsBase64(){
        return new Promise((resolve,reject)=>{        
            this.client.on('qr', (qr) => {
                if(this.contador>=this.timeoutTimes){
                    this.client.destroy();
                }else{
                    console.log('qr: ',this.contador," HASH:", qr);
                    this.encodeBase64(qr).then((base64)=>{
                        resolve(base64);
                    });
                }
                this.contador++;
            });
            this.initializeClient();
        })
    }
    /**
     * 
     * MULTIPLE WHATSAPP CALL FUNCTIONS
     * 
     */
    sendMessageText(chatId,message){
        this.client.sendMessage(chatId,message);
    }
    sendMessageMediaUrl(chatId,uri,caption,filename){
        let extension = uri.substring(uri.lastIndexOf("."));
        let media = MessageMedia.fromUrl(uri).then((resp)=>{
            media=resp;
            media.filename=filename+extension;
            // media.mimetype="application/*";
            this.client.sendMessage(chatId,media,{caption:caption});
        });
    }
    sendMessageStickerUrl(chatId,uri,caption){
        let extension = uri.substring(uri.lastIndexOf("."));
        let media = MessageMedia.fromUrl(uri).then((resp)=>{
            media=resp;
            // media.filename=req.body.filename+extension;
            // media.mimetype="application/*";
            this.client.sendMessage(chatId,media,{caption:caption,sendMediaAsSticker: true});
        });
    }
    sendMessageFileUrl(chatId,uri,filename){
        let extension = uri.substring(uri.lastIndexOf("."));
        let media = MessageMedia.fromUrl(uri).then((resp)=>{
            media=resp;
            media.filename=filename+extension;
            media.mimetype="application/*";
            this.client.sendMessage(chatId,media);
        });
    }
    getStatus(){
        return new Promise(async (resolve,reject)=>{
            let preBuildResponse={
                info:"Sin informacion",
                state:"Sin estado",
                battery:"Sin informacion de bateria",
                web:"Sin informacion de navegador",
                profilePicUrl:"Sin imagen",
                about:"Sin info"
            }
            // console.log("tempclientstatus: ",this.client.info);
            if(this.state=="disconnected"){
                preBuildResponse.state="DISCONNECTED";
                resolve(preBuildResponse);
            }
            if(this.client){
                try{
                    preBuildResponse.info = await this.client.info;
                    preBuildResponse.state = await this.client.getState();
                    preBuildResponse._state = await this.getState(preBuildResponse.state);
                    preBuildResponse.battery = await this.client.info.getBatteryStatus();
                    preBuildResponse.web = await this.client.getWWebVersion();
                    preBuildResponse.profilePicUrl = await this.client.getProfilePicUrl(this.client.info.wid._serialized);
                    // preBuildResponse.about = await this.client.getAbout();
                    preBuildResponse.about = "Hey there im using wahtsapp";
                    // console.log("this.client.info.wid: ",this.client.info.wid._serialized);
                    // console.log("preBuildResponse: ",preBuildResponse);
                    resolve(preBuildResponse);
                }catch(error){
                    reject(preBuildResponse);
                }

                // this.client.getState().then((state)=>{
                //     let info = await this.client.info.getBatteryStatus().then((battery)=>{
                //         resolve({info:this.client.info,state:state,battery:battery});
                //     }).catch(()=>{
                //         resolve({info:this.client.info,state:state,battery:"No info available"});
                //     })
                // })
            }else{
                reject(preBuildResponse)
            }
        })

    }
    setStatus(status){
        return new Promise((resolve,reject)=>{
            this.client.setStatus(status).then(()=>{
                resolve("Estado actualizado");
            }).catch(()=>{
                reject("Error actualizando estado");
            })
        })
    }
    setDisplayName(displayName){
        return new Promise((resolve,reject)=>{
            this.client.setDisplayName(displayName).then(()=>{
                resolve("Estado actualizado");
            }).catch(()=>{
                reject("Error actualizando estado");
            })
        })
    }
    getProfilePicUrl(){
        return new Promise((resolve,reject)=>{
            this.client.getProfilePicUrl().then((data)=>{
                resolve(data);
            }).catch(()=>{
                reject("Error obteniendo foto perfil");
            })
        })
    }
    getChats(){
        return new Promise((resolve,reject)=>{
            this.client.getChats().then((data)=>{
                resolve(data);
            }).catch(()=>{
                reject("Error obteniendo chats");
            })
        })
    }
    getGroups(){
        return new Promise((resolve,reject)=>{
            this.client.getChats().then((data)=>{
                var groups = data.filter(function (el) {
                    return el.isGroup == true; //&&
                  });
                resolve(groups);
            }).catch(()=>{
                reject("Error obteniendo grupos");
            })
        })
    }
    getBroadcast(){
        return new Promise((resolve,reject)=>{
            this.client.getChats().then((data)=>{
                var groups = data.filter(function (el) {
                    return el.id.server == "broadcast"; //&&
                  });
                resolve(groups);
            }).catch(()=>{
                reject("Error obteniendo grupos");
            })
        })
    }
}
module.exports = WhatssappClient;
