var FormData = require('form-data');
module.exports = function jsonToFormData(item) {
    var form_data = new FormData();
    for (var key in item ) {
        if(item[key]!=null){
            form_data.append(key, item[key]);
        }
    }
    return form_data;
}